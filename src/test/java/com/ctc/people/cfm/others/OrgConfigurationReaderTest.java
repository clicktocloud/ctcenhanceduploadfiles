package com.ctc.people.cfm.others;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.ctc.salesforce.enhanced.adaptor.SalesforceAgent;
import com.sforce.soap.partner.PartnerConnection;
import com.sforce.ws.ConnectionException;


public class OrgConfigurationReaderTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Ignore
	@Test
	public void testGetPartnerConnection_InSalesforceConnectionHelper() throws ConnectionException {
		PartnerConnection conn = SalesforceAgent.getConnectionByOAuth("00D90000000gtFz");
		
		
		System.out.println(conn.getUserInfo().getUserId());
	}
	
	@Test
	public void testBuild(){
		SalesforceAgent.build("00D90000000gtFz","");
	}

}
