package com.ctc.people.cfm.controller;


import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ctc.people.cfm.entity.ParseResumeToCandidateRequest;
import com.ctc.people.cfm.entity.ParseResumeToCandidateResponse;
import com.ctc.people.cfm.service.ParseResumeToCandidateService;

/**
 * Created by junzhou on 10/08/2016.
 */

@Controller
public class ImportSingleCandidateController {
    @Autowired
    private HttpServletRequest request;

    @RequestMapping(value = "/importCandidate", method=RequestMethod.POST, headers="Accept=application/json")
    public ResponseEntity<ParseResumeToCandidateResponse> importCandidate(@RequestBody ParseResumeToCandidateRequest request) throws Exception {
       
        ParseResumeToCandidateService iCandidateService = new ParseResumeToCandidateService();

        ParseResumeToCandidateResponse eServiceResponse = iCandidateService.parseToCandidate(request);
        
        return new ResponseEntity<ParseResumeToCandidateResponse>(eServiceResponse, HttpStatus.OK);
    }
}