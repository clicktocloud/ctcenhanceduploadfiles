package com.ctc.people.cfm.controller;

import java.io.File;
import java.io.IOException;
import java.rmi.ServerException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import com.ctc.entity.Candidate;
import com.ctc.people.cfm.adaptor.other.FilesRemover;
import com.ctc.people.cfm.component.SingleCandidateParseRunner;
import com.ctc.people.cfm.entity.CTCFile;
import com.ctc.people.cfm.entity.CTCFilesOwner;
import com.ctc.people.cfm.entity.Constant;
import com.ctc.people.cfm.monitor.CandidateFilesMonitor;
import com.ctc.people.cfm.util.CFMHelper;



public class ImportMultiCandidatesController extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static Logger logger = Logger.getLogger("com.ctc.people");
	
	private CandidateFilesMonitor importMultiCandidates  = new CandidateFilesMonitor() ;
	
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}
	
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServerException, IOException {
		FileItemFactory factory = new DiskFileItemFactory();
		ServletFileUpload upload = new ServletFileUpload(factory);
		List<FileItem> items = null;   												// all uploaded items
		File uploadedResume = null;											// the uploaded resume to be parsed
		Map<String, String> formFieldsMap = new HashMap<String, String>();	// to store form fields from upload request
		Map<String, HashMap<String, String>> errorMsgMap = new HashMap<String, HashMap<String,String>>();	// to store error messages	
		List<Thread> threadList = new ArrayList<Thread>();
		Map<String, Candidate> successfulCandidateMap = new HashMap<String, Candidate>();
		long startTime = System.currentTimeMillis();
		
		logger.info("=================================================");
		logger.info("Start of DaxtraMultiFilesParsingNew...");
		logger.debug("Maintenance?" + CFMHelper.getValueFromPropertyFile("isUnderMaitenance",Constant.PROPERTIES_FILENAME));
		logger.debug("Test?" + CFMHelper.getValueFromPropertyFile("isUnderTest",Constant.PROPERTIES_FILENAME));
		logger.debug("Namespace prefix:" + Constant.namespacePrefix);

		CTCFile resume= null;
		
		try{
			items =  upload.parseRequest(request);
			Iterator<FileItem> iter = items.iterator();

			while (iter.hasNext()) {
				FileItem item = iter.next();
				if (item.isFormField()) {
					
					// if the function is under maintenance, return a maintenance page.
					if(item.getFieldName().equals("urlprefix") && CFMHelper.getValueFromPropertyFile("isUnderMaitenance",Constant.PROPERTIES_FILENAME).equals("yes")){
						response.sendRedirect(item.getString()+"apex/ImportCandidateResult?code=88888");
						return;
					}
						
					// to store the information of the instance including org Id, bucket name, URL prefix
					formFieldsMap = CFMHelper.setFormField(item, formFieldsMap);
					
				} else if(item.getName() != ""){
					
					logger.debug("Instance: " + formFieldsMap.get("orgId"));
					logger.debug("URL Prefix: " + formFieldsMap.get("urlprefix"));
					logger.debug("Bucket Name: " + formFieldsMap.get("bucketName"));
					logger.debug("Skill Groups: " + formFieldsMap.get("skillGroups"));
					logger.debug("Resume: " + item.getName());
					
					String orgId = formFieldsMap.get("orgId");
					String bucketName = formFieldsMap.get("bucketName");
					String skillGroupsString = formFieldsMap.get("skillGroups");
					if(skillGroupsString == null){
						skillGroupsString = "";
					}
			
					
					// get daxtra account
					//DaxtraAccount daxtraAcc = RDSConnection.connectToRDS(formFieldsMap.get("orgId"));
					
					// to re-generate the file name for the resume being uploaded
					uploadedResume = CFMHelper.processUploadedFile(item);
					String resumeName = CFMHelper.fixWebDocFileName(item.getName());
					
					
					resume = new CTCFile();
					CTCFilesOwner owner = new CTCFilesOwner();
					resume.setOwner( owner );
					owner.getFiles().add(resume);
					
					owner.setOrgId( orgId );
					owner.setNamespace(Constant.namespacePrefix);
					owner.setEnableSkillParsing(true);
					owner.setSkillGroups( Arrays.asList(skillGroupsString.split(":")));
			        resume.setOriginalFileName(resumeName);
			        owner.setBucketName( bucketName );
			        resume.setNewFileName(uploadedResume.getName());
			        resume.setPath( uploadedResume.getAbsolutePath() );
			        //sfWebDoc.setWhoId(whoId);
			        owner.setWhoIdField(  Constant.namespacePrefix + "Document_Related_To__c");
			       // sfWebDoc.setWhatId(whatId);
			       // sfWebDoc.setWhatIdField(whatIdField);
			        resume.setType( "Resume" );
			        resume.setSize(FileUtils.byteCountToDisplaySize(uploadedResume.length()));
					
			        
					// create new threads for each resume
					SingleCandidateParseRunner isc = new SingleCandidateParseRunner(resume,errorMsgMap,successfulCandidateMap,threadList);
					Thread isct = new Thread(isc);
					isct.setName("importSingleCanidateThread_" + CFMHelper.getFileName(uploadedResume));
					
					// add the thread created to threadList 
					threadList.add(isct);
					isct.start();
					
					// wait here until all threads completed
					importMultiCandidates.scanAllThread(threadList);
					
					//remove temp resume file
					FilesRemover.remove( Arrays.asList( new CTCFile[]{resume}) );
					resume = null;
					
					// set redirect URL
					response.sendRedirect( getRedirectURL(successfulCandidateMap,errorMsgMap,formFieldsMap));
				}
			}
		}catch(FileUploadException e) {
			logger.error(e);
			response.sendRedirect(formFieldsMap.get("urlprefix")+"apex/ImportCandidateResult?code=99999");
		}finally{
			if(resume != null)
			FilesRemover.remove( Arrays.asList( new CTCFile[]{resume}) );
		}
		
		
		
    	// to calculate time consumed (for debug purposes)
    	long totalTimeSpent = System.currentTimeMillis() - startTime;  
    	logger.info("All completed:" + uploadedResume);
    	logger.info("Time Spent:" + totalTimeSpent/1000 + " s");
    	logger.info("End of the Main Thread.");
    	logger.info("=================================================");
	}
	
	
		
	/**
	 * To get the redirect URL according to the end results 
	 * 
	 * @param successfulCandidateMap
	 * @param errorMsgMap
	 * @param formFieldsMap
	 * @return
	 */
	private String getRedirectURL(Map<String, Candidate> successfulCandidateMap, Map<String, HashMap<String, String>> errorMsgMap, Map<String, String> formFieldsMap){
		// check all error messages generated.
		String errorCode = "";
		String cid4URL = "";
		String url = "";
		for(String cv: errorMsgMap.keySet()){
			if(errorMsgMap.get(cv).containsKey("NoEmailError")){
				errorCode = "10001";
				cid4URL = "";
				logger.debug("Error: " +  errorMsgMap.get(cv).get("ParserError"));
				break;
			}
			else if(errorMsgMap.get(cv).containsKey("S3Error")){
				errorCode = "10002";
				cid4URL = "";
				logger.debug("Error: " +  errorMsgMap.get(cv).get("S3Error"));
				break;
			}
			else if(errorMsgMap.get(cv).containsKey("SFError")){
				errorCode = "10003";
				cid4URL = "";
				logger.debug("Error: " +  errorMsgMap.get(cv).get("SFError"));
				break;
			}	
			else if(errorMsgMap.get(cv).containsKey("UsrError")){
				errorCode = "10000";
				cid4URL = errorMsgMap.get(cv).get("UsrError").split(":")[0];
				logger.debug("Error: " +  errorMsgMap.get(cv).get("UsrError"));
				logger.debug("Duplicate Candiate ID: " +  cid4URL);
				break;
			}
			else if(errorMsgMap.get(cv).containsKey("ParserError")){
				errorCode = "10004";
				cid4URL = successfulCandidateMap.keySet().toArray()[0].toString();
				logger.debug("Failed to parse first name or last name.");
				break;
			}
		}
		
		
		if(!successfulCandidateMap.keySet().isEmpty() && errorCode.isEmpty()){
			errorCode = "00000";
			cid4URL = successfulCandidateMap.keySet().toArray()[0].toString();
			logger.info("Successful Candidate ID: " +  cid4URL);
		}else if(errorCode.isEmpty()){
			logger.error("Some unexpected error occurred.\t Instance:" + formFieldsMap.get("orgid"));
			errorCode = "99999";
			cid4URL = "";
		}
		
		
		url = CFMHelper.getURL(formFieldsMap, errorCode,cid4URL);

		logger.info("Error Code: " +  errorCode);
		logger.info("URL: "+ url );
		return url;
	}
	
}
