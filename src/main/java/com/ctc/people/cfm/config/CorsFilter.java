package com.ctc.people.cfm.config;
import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpStatus;


public class CorsFilter implements Filter {
    public void destroy() {
    }
    public static String VALID_METHODS = "DELETE, HEAD, GET, OPTIONS, POST, PUT";

    private String allowedOrigin ="";
    private String allowSubDomain = "false";
    private String checkReferer = "false";
    private String referer = "";
   
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest httpReq = (HttpServletRequest) req;
        HttpServletResponse httpResp = (HttpServletResponse) resp;
        
        referer = httpReq.getHeader("Referer");
        boolean isAllowed = true;

        // No Origin header present means this is not a cross-domain request
        String origin = httpReq.getHeader("Origin");
         if (origin == null) {
            // Return standard response if OPTIONS request w/o Origin header
           if ("OPTIONS".equalsIgnoreCase(httpReq.getMethod())) {
                httpResp.setHeader("Allow", VALID_METHODS);
                httpResp.setStatus(200);
                return;
            }
        } else {
        	isAllowed = isAllowed(origin);
        	if( isAllowed){
        		// This is a cross-domain request, add headers allowing access
                httpResp.setHeader("Access-Control-Allow-Origin", origin);
                httpResp.setHeader("Access-Control-Allow-Methods", VALID_METHODS);

                String headers = httpReq.getHeader("Access-Control-Request-Headers");
                if (headers != null)
                    httpResp.setHeader("Access-Control-Allow-Headers", headers);

                // Allow caching cross-domain permission
                httpResp.setHeader("Access-Control-Max-Age", "3600");
        	}
            
        }
        // Pass request down the chain, except for OPTIONS
        if (!"OPTIONS".equalsIgnoreCase(httpReq.getMethod())) {
        	if( ! isAllowed ){
        		httpResp.setStatus(HttpStatus.SC_UNAUTHORIZED);
                return;
        	}else{
        		chain.doFilter(req, resp);
        	}
            
        }
 }

    public void init(FilterConfig config) throws ServletException {
    	
    	allowedOrigin = config.getInitParameter("cors.allowOrigin");
    	
    	System.out.println("allowedOrigin: " + allowedOrigin);
    	
    	allowSubDomain = config.getInitParameter("cors.allowSubdomains");
    	if(allowSubDomain != null && "true".equalsIgnoreCase(allowSubDomain)){
    		allowSubDomain = "true";
    	}else{
    		allowSubDomain = "false";
    	}
    	
    	checkReferer = config.getInitParameter("cors.checkReferer");
    	if(checkReferer != null && "true".equalsIgnoreCase(checkReferer)){
    		checkReferer = "true";
    	}else{
    		checkReferer= "false";
    	}
    	

    }
    
    private boolean isAllowed(String origin){
    	if(allowedOrigin == null || allowedOrigin =="" || allowedOrigin=="*"){
    		return true;
    	}
    	
    	System.out.println("Origin: " + origin);
    	
    	String[] allowedOrigins = allowedOrigin.split("\\s");
    
    	
    	for(String allowedOrigin : allowedOrigins){
    		
    		
    		if(allowSubDomain.equals("true")){
    			String allowedDomain = allowedOrigin.toLowerCase().replace("https://", "").replace("http://", "");
    			if(origin.toLowerCase().endsWith( allowedDomain)){
    				return true;
    			}
    		}else{
    			if(origin.toLowerCase().equals(allowedOrigin.toLowerCase())){
    				return true;
    			}
    		}
    		
    	}
    	
    	if("true".equals(checkReferer) && referer != null && !referer.equals("")){
    		
    		if(referer.toLowerCase().startsWith(origin.toLowerCase())){
    			return true;
    		}
    	}
    	
    	return false;
    }

}