/**
 * 
 */
package com.ctc.people.cfm.extension.controller;


import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.ctc.people.cfm.entity.ActivityDocs;
import com.ctc.people.cfm.entity.CTCFile;
import com.ctc.people.cfm.entity.CTCFilesOwner;
import com.ctc.people.cfm.entity.Constant;
import com.ctc.people.cfm.extension.component.CandidateFilesUploader;
import com.ctc.people.cfm.extension.component.FilesUploadRunner;
import com.ctc.people.cfm.extension.service.BasicCandidateMaker;
import com.ctc.people.cfm.util.CFMHelper;


/**
 * @author andy
 *
 */
@Controller
public class UploadCandidateFilesController {
	@Autowired
	private HttpServletRequest request;
	
	private static Logger logger = Logger.getLogger("com.ctc.people");
	
	
	@RequestMapping(method=RequestMethod.POST,value="/candidate-docs")
	public ResponseEntity<String> uploadFiles(@RequestBody String parameter){
		ResponseEntity<String> result = null;
		try{
			
			logger.info("\n===== Begin to upload candidate and files =====\n");
			//logger.info("Start handling request in controller " + this.getClass().getName());
			//get the value of "activityParam" parameter from request
			parameter=parameter.replace("activityParams=", "");
			String params = URLDecoder.decode(parameter, "UTF-8");
			
			//logger.info("Extracted activityParams : " + params);
			
			//build the list of ActivityDoc by the value of "activityParams" parameter
			ActivityDocs activityDocs = CFMHelper.fetchActivityDocsFromJson(params);
			
			logger.info("Built the list of activity doc : [ "+activityDocs.getActivityDocs().size() +" ]");
			
			//asynchronously execute uploading file process depending on the activityDocs 
			FilesUploadRunner fileUploadTask=new FilesUploadRunner( new CandidateFilesUploader(activityDocs) );
			ExecutorService executorService = Executors.newSingleThreadExecutor();
			//logger.info("Asynchronously executing CandidateFilesUploadRunner ...");
			executorService.submit( fileUploadTask );
			
			//
			result=new ResponseEntity<String>("We are processing your request.", HttpStatus.OK);
			
		}catch(Exception ex){
			
			logger.error("Errors in CTCPeopleFilesService "+ ex);
			ex.printStackTrace();
			result=new ResponseEntity<String>("Your request cannot be parsed. Please contact your administrator for help."
					, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		logger.info("===== Complete the request of uploading candidate and files =====");
		
		return result;
		
	}
	
	
	
	@RequestMapping(method=RequestMethod.GET,value="/reparse-resume")
	public ResponseEntity<String> reparseResume(
			@RequestParam("orgId") String orgId,
			@RequestParam("candidateId") String candidateId,
			@RequestParam("resumeName") String resumeName,
			@RequestParam("resumeKey") String resumeKey,
			@RequestParam(value="bucketName", required = false) String bucketName,
			@RequestParam("namespace") String namespace,
			@RequestParam("enabledSkillParsing") Boolean enabledSkillParsing){
		ResponseEntity<String> result = null;
		try{
			
			logger.info("\n===== Begin to reparse resume ["+resumeName+"] =====\n");
			logger.debug("orgId: " + orgId );
			logger.debug("candidateId: " + candidateId );
			logger.debug("resumeName: " + resumeName );
			logger.debug("resumeKey: " + resumeKey );
			logger.debug("bucketName: " + bucketName );
			logger.debug("namespace: " + namespace );
			logger.debug("enabledSkillParsing: " + enabledSkillParsing );
			
			
			//String params = URLDecoder.decode(parameter, "UTF-8");
			
			if(StringUtils.isEmpty(orgId) 
					|| StringUtils.isEmpty(candidateId)
					|| StringUtils.isEmpty(resumeName)
					|| StringUtils.isEmpty(resumeKey)
					){
				
				logger.info("Please input valid parameters");
				return new ResponseEntity<String>("Please input valid parameters."
						, HttpStatus.INTERNAL_SERVER_ERROR);
			}
			/*
			orgId = URLDecoder.decode(orgId, "UTF-8");
			candidateId = URLDecoder.decode(candidateId, "UTF-8");
			resumeName = URLDecoder.decode(resumeName, "UTF-8");
			resumeKey = URLDecoder.decode(resumeKey, "UTF-8");
			*/
			
			CTCFilesOwner filesOwner = new CTCFilesOwner();
			filesOwner.setOrgId(orgId);
			filesOwner.setNamespace(namespace);
			filesOwner.setWhoId(candidateId);
			filesOwner.setEnableSkillParsing(enabledSkillParsing);
			
			List<CTCFile> files = new ArrayList<CTCFile>();
			filesOwner.setFiles(files);
			
			CTCFile file = new CTCFile();
			file.setOwner(filesOwner);
			file.setOriginalFileName(resumeName);
			file.setNewFileName(resumeKey);
			file.setType(Constant.RESUME_DOC_TYPE);
			files.add(file);
			
			
			
			BasicCandidateMaker.make(Arrays.asList(new CTCFilesOwner[]{filesOwner}),bucketName);
			
			//
			result=new ResponseEntity<String>("We are processing your request.", HttpStatus.OK);
			
		}catch(Exception ex){
			
			logger.error(ex);
			
			result=new ResponseEntity<String>("Your request cannot be parsed. Please contact your administrator for help."
					, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		logger.info("===== Complete the request of reparsing resume =====");
		
		return result;
		
	}
	
	
}
