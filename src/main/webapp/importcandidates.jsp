<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Import Candidate</title>
<style type="text/css">
	div#sfinfo{
		border: solid red 1px
	}
	div#resume{
		border:solid green 1px;
		margin-top: 30px;
		margin-bottom: 20px;
	}
</style>
</head>
<body>
	<h1>Please Import Resume Here:</h1>
	<hr/>
	<form enctype="multipart/form-data" method="POST" action="/ctcenhanceduploadfiles/ImportMultiCandidates">
		<div id="sfinfo">
			<label for="sfurl">URL</label>
			<input type="text" name="urlprefix" id="sfurl" value="https://ap2.salesforce.com/"/><br/><br/>
			<label for="sforgid">Orgid</label>
			<input type="text" name="orgId" id="sforgid" value="00D28000000TdHk"/><br/><br/>
			<label for="sfbucketname">Bucket Name</label>
			<input type="text" name="bucketName" id="sfbucketname" value="temp003"/><br/><br/>
			<label for="sfskillgroups" >Skill Groups</label>

            <!--
			<select multiple name="skillgroups" id="skillgroups">
				<option value="Held Position">Held Position</option>
  				<option value="Skill">Skill</option>
  				<option value="Skill > Language">Skill > Language</option>
  				<option value="Degree/Qualification">Degree/Qualification</option>
  				<option value="Degree/Qualification > Professional">Degree/Qualification > Professional</option>
			</select>
            -->

            <select id="skillGroups" name="skillGroups" multiple size="6">
                <option value="G000001">Education/Quals</option>
                <option value="G000002">Industry</option>
                <option value="G000003">Position</option>
                <option value="G000004">Analysis / Consulting</option>
                <option value="G000005">Business Services /  NON IT</option>
                <option value="G000006">Candidate Hot List</option>
                <option value="G000007">Development Services</option>
                <option value="G000008">Infrastructure Services</option>
                <option value="G000009">Finance</option>
                <option value="G000010">Project Services</option>
                <option value="G000011">QA testing</option>
                <option value="G000012">SMS</option>
                <option value="G000013">Location</option>
                <option value="G000014">Held Position</option>
                <option value="G000015">SMS_Position</option>
            </select>
		</div>
		<div id="resume">
			<label for="resume">Resume</label>		
			<br>	
			<input type="file" name="resume1" id="resume1"/>
			<input type="file" name="resume2" id="resume2"/>
			<input type="file" name="resume3" id="resume3"/>

		</div>
			<input type="submit" name="submit" value="Import Candidate" id="importButton"/>
		
	</form>
</body>
</html>